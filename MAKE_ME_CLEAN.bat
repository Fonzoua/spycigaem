:: Deletes all files with those extensions in this folder (including sub-folders)

del /s *.booproj
del /s *.pidb
del /s *.userprefs
del /s *.unityproj
del /s *.sln
del /s *.suo
del /s *.user
del /s *.tmp
del /s *.svd
del /s *.pdb
del /s *.opendb
del /s *.pidb.meta
del /s *.pdb.meta
del /s *.apk
del /s *.unitypackage


REM If it says that the file can't be found it means there was no file with that extension in the folder and its ok
PAUSE