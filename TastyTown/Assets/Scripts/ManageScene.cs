﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum CurrentOpponent
{
    NONE = 0,
    REVIEW_BRO,
    JIMMY_BOI,
    BUDDY
}


public class ManageScene : MonoBehaviour
{
    private static ManageScene _instance;
    public static ManageScene Instance { get { return _instance; } }

    // Those are Scenes but Unity cant display scenes in inspector so we
    // gotta pretend they are objects
    [Header("Scenes")]
    [SerializeField]Object Title;
    [SerializeField] Object Transition;
    [SerializeField] Object Level1;
    [SerializeField] Object Level2;
    [SerializeField] Object Level3;
    [SerializeField] Object GameOver;
    [SerializeField] Object WinScene;

    private Object nextLevel;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
        DontDestroyOnLoad(this);

    }

    private void Start()
    {
        nextLevel = Level1;
        //Debug.Log("_+_" + nextLevel.name + " +___ " + Level1.name);
    }

    public CurrentOpponent GetOpponent()
    {
        if (SceneManager.GetActiveScene().name == Level1.name)
            return CurrentOpponent.REVIEW_BRO;
        else if (SceneManager.GetActiveScene().name == Level2.name)
            return CurrentOpponent.JIMMY_BOI;
        else if (SceneManager.GetActiveScene().name == Level3.name)
            return CurrentOpponent.BUDDY;

        return CurrentOpponent.NONE;
    }

    public CurrentOpponent GetNextOpponent()
    {
        if (nextLevel.name == Level1.name)
            return CurrentOpponent.REVIEW_BRO;
        else if (nextLevel.name == Level2.name)
            return CurrentOpponent.JIMMY_BOI;
        else if (nextLevel.name == Level3.name)
            return CurrentOpponent.BUDDY;

        return CurrentOpponent.NONE;
    }

    public void SwitchToNextScene()
    {
        if (SceneManager.GetActiveScene().name == GameOver.name)
            LoadToTransition();
        else if (SceneManager.GetActiveScene().name == WinScene.name)
            LoadToTitle();
        else
            SceneManager.LoadScene(nextLevel.name);
    }
    public void SwitchToLevelByName(string name)
    {
        SceneManager.LoadScene(name);
    }
    public void LoadToTransition()
    {
        SceneManager.LoadScene(Transition.name);
    }
    public void LoadToGameOver()
    {
        SceneManager.LoadScene(GameOver.name);
    }
    public void LoadToTitle ()
    {
        SceneManager.LoadScene(Title.name);
    }
    public void LoadToWin()
    {
        SceneManager.LoadScene(WinScene.name);
    }
    // Good naming conventions
    public void SetNextToNext()
    {
        Debug.Log(nextLevel.name);
        if (nextLevel.name == Level1.name)
            nextLevel = Level2;
        else if (nextLevel.name == Level2.name)
            nextLevel = Level3;
        else if (nextLevel.name == Level3.name)
            nextLevel = Level1;
        else
            Debug.LogError("You done fucked it up my dude");

    }



}
