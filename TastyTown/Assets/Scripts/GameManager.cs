﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum GAME_STATE
{
    NONE = 0,
    GAME_OVER,
    GAME_TRANSITION,
    GAME_LEVEL,
    GAME_TITLE,
    GAME_WIN
}



public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    [Header("Game Settings")]
    [Header("How long before the game is restarted")]
    public float endGameWaitTime = 1.0f;
    [Header("Min/Max values for Player food amounts")]
    public float playerFoodMin = 0.0f;
    public float playerFoodMax = 100.0f;
    [Header("Min/Max values for Enemy food amounts")]
    public float enemyFoodMin = 0.0f;
    public float enemyFoodMax = 100.0f;

    [SerializeField] private float Player_Food_Meter = 0.0f;
    [SerializeField] private float Enemy_Food_Meter = 0.0f;
    

    [Header("Game's state")]
    [SerializeField] private GAME_STATE currentState;
    [Header("Key bindings")]
    public KeyCode proceedKey;

    private bool isPressedKey;
    private bool isEndGameRunning; 
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        isEndGameRunning = false;
        isPressedKey = false;
    }

    public float GetPlayerMeter()
    {
        return Player_Food_Meter;
    }

    public float GetEnemyMeter()
    {
        return Enemy_Food_Meter;
    }

    public void IncrementPlayerFoodMeter(float amount)
    {
        Player_Food_Meter += amount;
        Mathf.Clamp(Player_Food_Meter, playerFoodMin, playerFoodMax);
    }


    public void DecrementPlayerFoodMeter(float amount)
    {
        Player_Food_Meter -= amount;
        Mathf.Clamp(Player_Food_Meter, playerFoodMin, playerFoodMax);
    }

    public void IncrementEnemyFoodMeter(float amount)
    {
        Enemy_Food_Meter += amount;
        Mathf.Clamp(Enemy_Food_Meter, enemyFoodMin, enemyFoodMax);
    }
    //lose condiiton is literally just enemy fills meter first
    //win is if you get to full first

    // Use this for initialization
 

    void Update()
    {
        switch (currentState)
        {
            case GAME_STATE.GAME_TITLE:
                if (Input.GetKeyUp(proceedKey) && !isPressedKey)
                {
                    isPressedKey = true;
                    EndGame(true);
                }
                break;

            case GAME_STATE.GAME_LEVEL:
                if (Player_Food_Meter >= 100)
                {
                    if (ManageScene.Instance.GetOpponent() == CurrentOpponent.BUDDY)
                    {
                        currentState = GAME_STATE.GAME_WIN;
                        ManageScene.Instance.LoadToWin();
                    }
                    else
                    {
                        EndGame(true);
                    }
                }
                else if (Enemy_Food_Meter >= 100)
                {
                    EndGame(false);
                }
                break;

            case GAME_STATE.GAME_TRANSITION:
                // Load next scene
                if (Input.GetKeyUp(proceedKey) && !isPressedKey)
                {
                    isPressedKey = true;
                    EndGame(true);
                }
                break;

            case GAME_STATE.GAME_OVER:
                if (Input.GetKeyUp(proceedKey))
                {
                    isPressedKey = true;
                    EndGame(true);
                }
                break;

            case GAME_STATE.GAME_WIN:
                if (Input.GetKeyUp(proceedKey))
                {
                    isPressedKey = true;
                    EndGame(true);
                }
                break;
                

            case GAME_STATE.NONE:
                Debug.LogError("You fucked up");
                break;

            default:
                Debug.LogError("You fucked up real bad");
                break;
        }
    }

    public void EndGame(bool win)
    {
        if (!isEndGameRunning)
        {
            isEndGameRunning = true;
            StartCoroutine(EndGameRoutine(win));
        }
    }

    IEnumerator EndGameRoutine(bool win)
    {
        //do shit for if the game is active
        yield return new WaitForSeconds(endGameWaitTime);
        RestartGame(win);
    }

    void RestartGame(bool win)
    {
        isPressedKey = false;
        isEndGameRunning = false;
        Player_Food_Meter = 0;
        Enemy_Food_Meter = 0;

        if (win)
        {
            switch (currentState)
            {
                case GAME_STATE.GAME_TITLE:
                    currentState = GAME_STATE.GAME_TRANSITION;
                    ManageScene.Instance.LoadToTransition();
                    break;
                case GAME_STATE.GAME_LEVEL:
                    currentState = GAME_STATE.GAME_TRANSITION;
                    ManageScene.Instance.SetNextToNext();
                    ManageScene.Instance.LoadToTransition();
                    break;
                case GAME_STATE.GAME_TRANSITION:
                    currentState = GAME_STATE.GAME_LEVEL;
                    ManageScene.Instance.SwitchToNextScene();
                    break;
                case GAME_STATE.GAME_OVER:
                    currentState = GAME_STATE.GAME_TITLE;
                    ManageScene.Instance.LoadToTitle();
                    break;
                case GAME_STATE.GAME_WIN:
                    ManageScene.Instance.SetNextToNext();
                    currentState = GAME_STATE.GAME_TITLE;
                    ManageScene.Instance.LoadToTitle();
                    break;
                case GAME_STATE.NONE:
                    Debug.LogError("Super goofed");
                    break;
                default:
                    Debug.LogError("Supersayan goof");
                    break;
            }
        }
        else
        {
            currentState = GAME_STATE.GAME_OVER;
            ManageScene.Instance.LoadToGameOver();
            // big succcccccc
        }
    }

}
