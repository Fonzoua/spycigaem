﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Food : MonoBehaviour
{
    // -- VARIABLES -- //
    public enum States
    {
        FALLING,
        LANDED,
        TYPING,
        DYING
    }

    public States currState;

    // Temp?
    public WordBank wb;
    private FoodSpawner Manager;
    private ComboManager ComboMngr;
    private GameManager GameMan;


    

    [Header("Text Position")]
    public Transform StartPosition;
    public Text currWord;
    public Text backgroundWord;
    //public GameObject wordFrame; 

    public string SampleText;

    public float EndPosition = -0.75f;
    public float Speed = -0.3f;
    public bool isPerfect = true;

    int SFXChoice;

    // -- METHODS -- //
    void Start()
    {
        Manager = GameObject.FindGameObjectWithTag("FoodSpawner").GetComponent<FoodSpawner>();
        ComboMngr = GameObject.FindGameObjectWithTag("ComboManager").GetComponent<ComboManager>();
        GameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        //RectTransform temp = wordFrame.GetComponent<RectTransform>();
        //temp.sizeDelta = new Vector2(currWord.text.Length * currWord.fontSize + 20, currWord.fontSize + 25);
        currWord.fontSize = Manager.textFontSize;
        backgroundWord.fontSize = Manager.textFontSize;

        // Set Text
        SampleText = wb.GetWord();

        // Initially falling
        currState = States.FALLING;

        // Texts
        currWord.color = Manager.InProgress;
        currWord.text = SampleText;
        currWord.enabled = false;
        
        backgroundWord.enabled = false;
        backgroundWord.text = SampleText;
        backgroundWord.color = Manager.BackGroundTextColor;

        // Initial position will be from spawner
        transform.position = StartPosition.position;
        EndPosition = StartPosition.position.y - Manager.fallDistance;
        Speed = -0.3f;
    }

    // Update is called once per frame
    void Update()
    {
        CheckState();
    }

    void CheckState()
    {
        if (currState == States.FALLING)
        {
            Fall();
        }
        else if (currState == States.LANDED)
        {
            SpawnText();
        }
        else if (currState == States.TYPING)
        {
            CheckForTyping();
        }
        else if (currState == States.DYING)
        {
            // Ded
        }
    }

    void Fall()
    {
        // Slowly move downward
        if (transform.position.y > EndPosition)
        {
            transform.Translate(new Vector3(0.0f, Speed, 0.0f));
        }

        // If reached destination
        if (transform.position.y <= EndPosition)
        {
            // Set Position if there's some offset
            transform.position = new Vector3(transform.position.x, EndPosition, transform.position.z);

            AudioManager.Instance.PlaySFX(AUDIO_SFX.SND_PLATE_LAND, Random.Range(0.9f, 1.1f));

            // Update state
            currState = States.LANDED;
        }
    }

    void SpawnText()
    {
        // Update the current word and Background Word
        currWord.enabled = true;
        backgroundWord.enabled = true;

        // Update state
        currState = States.TYPING;
    }

    void CheckForTyping()
    {
        // Is something pressed?
        if (Input.anyKey)
        {
            // Check if it's equal
            foreach (char i in Input.inputString)
            {
                CompareLetters(i);
            }
        }
    }

    void CompareLetters(char letter)
    {
        // If correct letter pressed
        if (letter == currWord.text[0])
        {
            // Remove letter and update color
            currWord.text = currWord.text.Substring(1);
            currWord.color = Manager.InProgress;

            SFXChoice = Random.Range(14, 18);
            AudioManager.Instance.PlaySFX((AUDIO_SFX)SFXChoice);

            //print(letter);
        }
        else
        {
            // Hurt player or whatever
            //print("incorrect letter");
            isPerfect = false;
            ComboMngr.ComboBreak();

            SFXChoice = Random.Range(18, 20);
            AudioManager.Instance.PlaySFX((AUDIO_SFX)SFXChoice);

        }

        // No more text?
        if (currWord.text == "")
        {
            // No more words
            currWord.enabled = false;
            backgroundWord.enabled = false;

            GameMan.IncrementPlayerFoodMeter(5.0f);

            // New State
            currState = States.DYING;
            if (isPerfect)
            {
                // Call combo manager add
                ComboMngr.ComboAdd();
                GameMan.IncrementPlayerFoodMeter(50.0f);
            }

            
            SFXChoice = Random.Range(0, 3);
            AudioManager.Instance.PlaySFX((AUDIO_SFX)SFXChoice);

            // Notify whichever manager
            Manager.SpawnFood();
            Destroy(gameObject);

            //print("WE DEAD");
        }
    }
}