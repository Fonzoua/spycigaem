﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1Boss : MonoBehaviour
{
    // -- VARIABLES -- //
    public EnemyFood theFood;
    public WordBank wb;

    [Header("Range for eating food")]
    public float low = 3.0f;
    public float high = 5.0f;

    // Use this for initialization
    void Start()
    {
        // Set up the Word Bank
        wb.currState = WordBank.States.ReviewBro;

        // Speed is dependent on boss
        theFood.timeRangeLow = low;
        theFood.timeRangeHigh = high;
    }
}
