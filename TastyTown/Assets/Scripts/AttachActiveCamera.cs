﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachActiveCamera : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //this.gameObject.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
        this.gameObject.GetComponent<Canvas>().worldCamera = Camera.main;
    }

}