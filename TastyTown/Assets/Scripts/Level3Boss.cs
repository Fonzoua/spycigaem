﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3Boss : MonoBehaviour
{
    /// -- VARIABLES -- //
    public EnemyFood theFood;
    public WordBank wb;

    [Header("Range for eating food")]
    public float low = 1.0f;
    public float high = 3.0f;

    // -- METHODS -- //
    void Start()
    {
        // Set up the Word Bank
        wb.currState = WordBank.States.Buddy;

        // Speed is dependent on boss
        theFood.timeRangeLow = low;
        theFood.timeRangeHigh = high;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
