﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboManager : MonoBehaviour
{
    public float CurrCombo = 0.0f;
    //public GameObject COMBO;
    public Text comboText;
    public Color comboTextColor;
    private Color storedColor;

    void Awake()
    {
        CurrCombo = 0.0f;
        Debug.Log(comboText.text + "YEEEEEEEEEEEEEEEEEEEEEET");
        //comboTextColor = Color.black;
        storedColor = comboTextColor;
        comboText.color = comboTextColor;
        comboText.text = "";
        comboText.enabled = true;
    }
    
    public void ComboAdd()
    {
        CurrCombo++;
        comboText.text = "Combo: " + CurrCombo.ToString();
        //comboText.color = comboTextColor * (Color.red * 0.01f);
        //print("Combo: " + CurrCombo.ToString());
    }

    public void ComboBreak()
    {
        CurrCombo = 0;
        comboText.text = "";
        comboText.color = storedColor;
        //print("BREAK:::: Combo: " + CurrCombo.ToString());
    }
}
