﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordBank : MonoBehaviour
{
    public enum States { ReviewBro, JimmyCalm, JimmyMad, Buddy }
    public States currState = States.ReviewBro;

    public void SetCurrState(States state)
    {
        currState = state;
    }

    public string GetWord()
    {
        // Word bank is dependent on State
        if (currState == States.ReviewBro)
        {
            // Return a random word
            return ReviewBroWords[Random.Range(0, ReviewBroWords.Length)];
        }
        else if (currState == States.JimmyCalm)
        {
            print("STATE IS CALM");
            return JimmyCalmWords[Random.Range(0, JimmyCalmWords.Length)];

        }
        else if (currState == States.JimmyMad)
        {
            print("STATE IS MAD");
            return JimmyEnragedWords[Random.Range(0, JimmyEnragedWords.Length)];
        }
        else if (currState == States.Buddy)
        {
            return "buddy";
        }

        return "BIG ERROR";
    }

    public string[] ReviewBroWords =
    {
        "A crispy crunch",
        "Running on empty",
        "Food Review",
        "Insurance Fraud",
        "Greasy Wrapping Paper",
        "Dad ruined Christmas",
        "Immeasurable disappointment",
        "Ruined day.",
        "Can I get uhhh..",
        "17 Chicken Sandwiches",
        "Oh boy 3 A.M",
        "Wacky Weeaboo",
        "Tentacion",
        "McReimbursement",
        "Kentucky Fried Sadness",
        "Grandma's House",
        "Suit and Tie",
        "George Foreman",
        "Denny's Disaster",
        "Alaskan Bull Worm",
        "Texas Trap House",
        "Stock exchange reject",
        "Ice cream machine",
        "Bottom text",
        "Washington",
        "Bottomless",
        "Ping-Pong",
        "Multithreading",
        "Angry Keenan",
        "Washed Up",
        "Green juice",
        "Brownie Points",
        "Twice cooked pork",
        "Cupid's Cockles",
        "Magician's Red",
        "Pistachio",
        "Ratatouille",
        "How do I eat this?",
        "Danny Doritos",
        "Pledge of Allegiance",
        "Cheeseburger Apocalypse",
        "Seinfeld Box Set",
        "Damn is a 7/10",
        "Floor Pizza",
        "Filipino Frenzy",
        "Sick Anime OSTs",
        "Fidget Fingers",
        "Pink Polo",
        "I'm gonna be sick",
        "Endless Appetizers",
        "Wow! Sugoi Sensei!!",
        "OK!",
        "Now this is podcasting!",
        "Sweet Release",
        "Stonehenge"
    };

    public string[] JimmyCalmWords =
    {
        "Is that a Jo-Joke?",
        "What were in those brownies?",
        "It's so moist",
        "I lied.",
        "Where we dropping?",
        "taco Tuesday",
        "Tasty Town",
        "not the father",
        "where is dad?",
        "whomst've",
        "stay home.",
        "The boys are back.",
        "Down with the sickness.",
        "A new hope.",
        "Cooler than cool.",
        "Alright alright alright.",
        "No job security.",
        "Can't think of another.",
        "Give up son.",
        "Dog underwear",
        "Big Smoke's Order.",
        "Dennys Menu",
        "ULTRALIGHT BEAM",
        "Destructive Diabetic",
        "It's a not a phase.",
        "Leave me alone Shaun!",
        "Miserable Overnight",
        "Bitcoin will NEVER crash, bro!",
        "Dedotated Wam",
        "Sweaty shirt stains"
    };

    public string[] JimmyEnragedWords =
    {
        "WOOo WOOW WOOoo",
        "WoOW wOoW WoOOW",
        "WOoW WOOw WoOW",
        "WeE WoO",
        "POOPITY SCOOP",
        "SCOOPITY WOOP",
        "POOPITY SCOOP",
        "POOPITY POOP?",
        "MOIST",
        "AUUGGH",
        "I'M GOING BERSERK",
        "WOWOWOW",
        "BLBLBLGRAHG",
        "I'M BACK!",
        "WEEEE WOOO",
        "PUWEEEE WOOO",
        "AGGGAHAHA",
        "WEEEEEEEE",
        "WOOO O O O",
        "YEEEEOWWWW",
        "BLAARH",
        "GGUUAG",
        "(Miscellaneous digestive noises)"
    };
}