﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarManager : MonoBehaviour
{
    
    public Slider PlayerProgress;
    public Slider EnemyProgress;

    // Use this for initialization
    void Start()
    {
        // Grab objects
        PlayerProgress = GameObject.FindGameObjectWithTag("PlayerSlider").GetComponent<Slider>();
        EnemyProgress = GameObject.FindGameObjectWithTag("EnemySlider").GetComponent<Slider>();

        // Set values based on GM
        PlayerProgress.maxValue = GameManager.Instance.playerFoodMax;
        EnemyProgress.maxValue = GameManager.Instance.enemyFoodMax;
    }

    // Update is called once per frame
    void Update()
    {
        // Update values
        PlayerProgress.value = GameManager.Instance.GetPlayerMeter();
        EnemyProgress.value = GameManager.Instance.GetEnemyMeter();
    }
}
