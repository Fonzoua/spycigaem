﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugManager : MonoBehaviour
{
    private static DebugManager _instance;
    public static DebugManager Instance { get { return _instance; } }

    public enum PRESET
    {
        PRESET_1 = 0,
        PRESET_2,
        PRESET_3
    }

    public PRESET preset = PRESET.PRESET_1;

    private Text DebugText1 = null;
    private Text DebugText2 = null;
    private Text DebugText3 = null;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
        DontDestroyOnLoad(this);

    }
    private void Update()
    {
        DebugText1 = GameObject.FindGameObjectWithTag("DebugText1").GetComponent<Text>();
        DebugText2 = GameObject.FindGameObjectWithTag("DebugText2").GetComponent<Text>();
        DebugText3 = GameObject.FindGameObjectWithTag("DebugText3").GetComponent<Text>();
        switch (preset)
        {
            case PRESET.PRESET_1:
                DebugText1.text = "Scene: " + SceneManager.GetActiveScene().name;
                break;
            case PRESET.PRESET_2:
                DebugText1.text = "Scene: " + SceneManager.GetActiveScene().name;
                break;
            case PRESET.PRESET_3:
                DebugText1.text = "Scene: " + SceneManager.GetActiveScene().name;
                break;

        }

        
    }

}
