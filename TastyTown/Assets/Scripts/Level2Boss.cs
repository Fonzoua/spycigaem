﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Boss : MonoBehaviour
{
    public enum States
    {
        NORMAL,
        ENRAGED
    }

    public States currState;
    public EnemyFood theFood;
    public WordBank wb;

    [Header("Timings")]
    public float timeBeforeRage;
    public float timeBeforeCalm;

    [Header("Range for eating food")]
    public float low = 2.5f;
    public float high = 8.0f;

    [Header("Enranged range")]
    public float enragedLow = 0.5f;
    public float enragedHigh = 2.0f;

    // Use this for initialization
    void Start()
    {
        // Initial State
        currState = States.NORMAL;
        wb.currState = WordBank.States.JimmyCalm;

        // Range for eating food
        low = 2.5f;
        high = 8.0f;

        // Enraged range
        enragedLow = 0.5f;
        enragedHigh = 2.0f;

        // Speed is dependent on boss
        theFood.timeRangeLow = low;
        theFood.timeRangeHigh = high;

        // Time he will remain enraged
        timeBeforeRage = Time.time + 5.0f;
    }

    // Update is called once per frame
    void Update()
    {
        CheckState();
    }

    void CheckState()
    {
        if (currState == States.NORMAL)
            EatNormally();
        else if (currState == States.ENRAGED)
            ConsumeAll();
    }

    void EatNormally()
    {
        if (Time.time > timeBeforeRage)
        {
            // Time to Enrage
            currState = States.ENRAGED;

            // Update the word bank
            wb.currState = WordBank.States.JimmyMad;

            // Update for enrage
            theFood.timeRangeLow = enragedLow;
            theFood.timeRangeHigh = enragedHigh;

            // Set time before calm
            timeBeforeCalm = Time.time + 10.0f;

            print("JIMMY MAD");
        }
    }

    void ConsumeAll()
    {
        if (Time.time > timeBeforeCalm)
        {
            // Time to calmdown
            currState = States.NORMAL;

            // Update the word bank
            wb.currState = WordBank.States.JimmyCalm;

            // Update for enrage
            theFood.timeRangeLow = low;
            theFood.timeRangeHigh = high;

            // Set time before calm
            timeBeforeRage = Time.time + 20.0f;

            print("JIMMY calm");
        }
    }

}
