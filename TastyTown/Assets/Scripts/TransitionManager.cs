﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionManager : MonoBehaviour
{
    private Text runningText;
    [SerializeField] private float scrollDelay = 0.05f;
    private bool isTalking = true;
    
    public void StartScroll(string dialogueText, Text textComponent)
    {
        runningText = textComponent;
        isTalking = true;
        StartCoroutine(AnimateText(dialogueText));
    }

    IEnumerator AnimateText(string dialogueText)
    {
        // Clearout text
        runningText.text = "";

        float accumTime = 0.0f;
        int c = 0;

        // The 'typing' effect
        while (isTalking && c < dialogueText.Length)
        {
            yield return null;
            accumTime += Time.deltaTime;

            while (accumTime > scrollDelay)   
            {
                accumTime -= scrollDelay;

                // if any text elft to type
                if (c < dialogueText.Length)
                    runningText.text += dialogueText[c];

                c++;
            }

        }

        runningText.text = dialogueText;
        isTalking = false;
    }

}
