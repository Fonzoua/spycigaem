﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OpponentInfoTransition : MonoBehaviour
{
    TransitionManager scrollingTextScript;

    [Header("Opponent names and flaworText")]
    [Header("Order: 1. ReviewBrah, 2. Joey, 3. Buddy ")]
    public string[] opponentNamesString;
    [TextArea] public string[] opponentFlaworString;

    [Header("Opponent Sprites (Same order)")]
    public Sprite[] opponentSprites;

    [Header("Display texts")]
    public Text OpponentName;
    public Text OpponentFlaworText;
    [Header("Display Image")]
    public Image OpponentImage;

	void Awake ()   
    {
        scrollingTextScript = this.GetComponent<TransitionManager>();
	}

    private void Start()
    {
        switch (ManageScene.Instance.GetNextOpponent())
        {
            case CurrentOpponent.REVIEW_BRO:
                OpponentName.text = opponentNamesString[0];
                scrollingTextScript.StartScroll(opponentFlaworString[0], OpponentFlaworText);
                OpponentImage.sprite = opponentSprites[0];
                break;
            case CurrentOpponent.JIMMY_BOI:
                OpponentName.text = opponentNamesString[1];
                scrollingTextScript.StartScroll(opponentFlaworString[1], OpponentFlaworText);
                OpponentImage.sprite = opponentSprites[1];
                break;
            case CurrentOpponent.BUDDY:
                OpponentName.text = opponentNamesString[2];
                scrollingTextScript.StartScroll(opponentFlaworString[2], OpponentFlaworText);
                OpponentImage.sprite = opponentSprites[2];
                break;
            default:
                OpponentName.text = "Error";
                scrollingTextScript.StartScroll("Andre is disapointed", OpponentFlaworText);
                OpponentImage.sprite = opponentSprites[0];
                Debug.LogError("You done goofed.   ManageScene.Instance.GetOpponent() returned " + ManageScene.Instance.GetOpponent().ToString());
                break;
        }
    }



}
