﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFoodSpawner : MonoBehaviour
{
    public EnemyFood badFood;
    public float FallDistance = 5.0f;

    void Start()
    {
        SpawnBadFood();
    }
    public void SpawnBadFood()
    {
        // Spawn a bad food
        Instantiate(badFood, transform.position, transform.rotation);
        AudioManager.Instance.PlaySFX(AUDIO_SFX.SND_PLATE_FALL, Random.Range(0.9f, 1.1f));
    }
}
