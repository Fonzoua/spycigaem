﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFood : MonoBehaviour
{
    // -- VARIABLES -- //
    public enum States { Falling, Landed, Eating, Dying }
    public States currState;

    // Temp?
    public EnemyFoodSpawner Manager;
    private GameManager GameMan;


    private float EndPosition;
    public float Speed;

    public float timer;

    public float timeRangeLow;
    public float timeRangeHigh;

    private CurrentOpponent currOp;

    int SFXChoie;

    // -- METHODS -- //
    void Start()
    {
        Manager = GameObject.FindGameObjectWithTag("EnemyFoodSpawner").GetComponent<EnemyFoodSpawner>();
        GameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        currOp = ManageScene.Instance.GetOpponent();
        // Initially falling
        currState = States.Falling;

        // Initialize other variables

        EndPosition = Manager.transform.position.y - Manager.FallDistance; // y-position
        Speed = -0.3f;          // Speed downward
        timeRangeLow = 3.0f;
        timeRangeHigh = 8.0f;
    }


    // Update is called once per frame
    void Update()
    {
        CheckState();
    }

    void CheckState()
    {
        if (currState == States.Falling)
        {
            Fall();
        }
        else if (currState == States.Landed)
        {
            // Update timer
            timer = Time.time + Random.Range(timeRangeLow, timeRangeHigh);

            // Change state
            currState = States.Eating;
        }
        else if (currState == States.Eating)
        {
            EatFood();
        }
        else if (currState == States.Dying)
        {

        }
    }

    void Fall()
    {
        // Slowly move downward
        if (transform.position.y > EndPosition)
        {
            transform.Translate(new Vector3(0.0f, Speed, 0.0f));
        }

        // If reached destination
        if (transform.position.y <= EndPosition)
        {
            // Set Position if there's some offset
            transform.position = new Vector3(transform.position.x, EndPosition, transform.position.z);

            AudioManager.Instance.PlaySFX(AUDIO_SFX.SND_PLATE_LAND, Random.Range(0.9f, 1.1f));

            // Update state
            currState = States.Landed;
        }
    }

    void EatFood()
    {
        // Has he finished his food?
        if (timer < Time.time)
        {
            // New State
            currState = States.Dying;

            GameMan.IncrementEnemyFoodMeter(1.0f);

            if(currOp == CurrentOpponent.REVIEW_BRO)
            {
                SFXChoie = Random.Range(3, 6);
            }
            else if (currOp == CurrentOpponent.JIMMY_BOI)
            {
                SFXChoie = Random.Range(6, 9);
            }
            else if (currOp == CurrentOpponent.BUDDY)
            {
                SFXChoie = Random.Range(9, 12);
            }

            AudioManager.Instance.PlaySFX((AUDIO_SFX)SFXChoie, Random.Range(0.9f, 1.1f));


            // Notify whichever manager and destroy
            Manager.SpawnBadFood();
            Destroy(gameObject);

            print("Enemy Food eaten");

            
        }
    }
}