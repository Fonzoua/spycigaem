﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    public Food theFood;
    public float fallDistance = 5.0f;
    public int textFontSize = 50;

    [Header("Text Colors")]
    public Color BackGroundTextColor = Color.grey;
    public Color InProgress = Color.red;

    void Start()
    {
        SpawnFood();
    }
    public void SpawnFood()
    {
        // Spawns a food when called
        Instantiate(theFood, transform.position, transform.rotation);
        //int SFXCHOICE;
        //SFXCHOICE = Random.Range(17, 20);
        AudioManager.Instance.PlaySFX(AUDIO_SFX.SND_PLATE_FALL, Random.Range(0.9f, 1.1f));
    }


}
